package com.dev.user.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManager {

    public Connection connection;
    public Statement statement;
    public ResultSet rs;
    public PreparedStatement prepared_statement;
	private PreparedStatement ps;
	private Statement st;
    public String sql;
    
    private static String _JDBC_DRIVER;
    private static String _USERNAME;
    private static String _PASSWORD;
    private String _DATABASE;
    private String _CONNECTION_STRING;
    
    private String sql_command;
    private String content;
    
    public DatabaseManager(){
        connection = null;
        statement = null;
        rs = null;
        sql = null;
        _JDBC_DRIVER = "com.mysql.jdbc.Driver";
        _USERNAME = "root";
        _PASSWORD = "";
        _DATABASE = "hnb_app_tecajna_lista";
        _CONNECTION_STRING = "jdbc:mysql://localhost:3306/"+_DATABASE+"?useUnicode=true&characterEncoding=UTF-8";
        
        /*try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.out.println("DriverManager nije registrirao driver za JDBC");
			e1.printStackTrace();
		}*/
        
        //System.out.println("Testiranje spoja na MySQL...");
        try {
            Class.forName(_JDBC_DRIVER);
            //System.out.println("Driver za MySQL "+_JDBC_DRIVER+" je registrian i konekcije su omogućene ");
        } catch (ClassNotFoundException e){
            System.out.println("Ne postoji JDBC driver za MySQL na serveru ili vašem računalu.");
            e.printStackTrace();
        }
    }
    
    public Connection getConnection(){
        
        connection = null;
        
        try {
        	connection = DriverManager.getConnection(_CONNECTION_STRING, _USERNAME, _PASSWORD);
        } catch (SQLException e){
            System.out.println("Konekcija se nije izvršila: provjerite output konzolu.");
        } finally {
            if(connection != null){
                //System.out.println("Konekcija na bazu je uspješno ostvarena.");
            } else {
                System.out.println("Konekcija na bazu se nije izvršila: provjerite da li je vaša baza uopće postoji.");
            }
        }

        return connection;
        
    }

}
