package com.dev.user.dao;

public interface DbSave {

	public void save_to_db(String table, String[] fields, String[] types, Object[] values);
	
}
