package com.dev.user.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedHashMap;

import com.dev.user.dao.DatabaseManager;
import com.dev.user.dao.DbSave;

public class TecajnaLista implements DbSave {

	private String sql;
	private PreparedStatement ps;
	private DatabaseManager db;
	private Connection connection;
	
	public Integer id;
	public String sifra;
	public String valuta;
	public String jedinica;
	public String kupovni;
	public String srednji;
	public String prodajni;
	public Date on_date;
	public Date created;
	
	@Override
	public void save_to_db(String table, String[] fields, String[] types, Object[] values) {
		// TODO Auto-generated method stub
		try {
			
			sql = " insert into "+table+" set ";
			int j = 0;
			for(int i = 0; i < fields.length; i++)
			{
				//System.out.println(fields[i]+", "+types[i]+", "+values[i]);
				String z_dod = (j > 0) ? ", " : "";
				sql += z_dod+" "+fields[i]+" = ? ";
				j++;
			}
			ps = (PreparedStatement) connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			
			for(int i = 0; i < fields.length; i++)
			{
				if(types[i].equals("string"))
				{
					ps.setString((i+1), (String) values[i]);
				}
				else if(types[i].equals("date"))
				{
					ps.setDate((i+1), (java.sql.Date) values[i]);
				}
				else if(types[i].equals("timestamp"))
				{
					ps.setTimestamp((i+1), (Timestamp) values[i]);
				}
				j++;
			}
			//System.out.println(ps);
			ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public TecajnaLista()
	{
		//
	}
	
	public TecajnaLista(String sifra, String valuta, String jedinica, String kupovni, String srednji, String prodajni, Date on_date)
	{
		db = new DatabaseManager();
        connection = db.getConnection();
        
		//this.id = id;
		this.sifra = sifra;
		this.valuta = valuta;
		this.jedinica = jedinica;
		this.kupovni = kupovni;
		this.srednji = srednji;
		this.prodajni = prodajni;
		this.on_date = on_date;
		this.created = created;
		
		try {
			sql = "SELECT id FROM tecaj where valuta = ? AND on_date = ? limit 1 ";
			//System.out.println(sql);
			ps = (PreparedStatement) connection.prepareStatement(sql);
			ps.setString(1, this.valuta);
			ps.setDate(2, (java.sql.Date) this.on_date);
			//System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			//String created = "";
			if (!rs.next()) {
				
				String[] fields = {"sifra","valuta","jedinica","kupovni","srednji","prodajni","on_date","created"};
				String[] types = {"string","string","string","string","string","string","date","timestamp"};
				Object[] values = {this.sifra, this.valuta, this.jedinica, this.kupovni, this.srednji, this.prodajni, (java.sql.Date) this.on_date, new Timestamp(System.currentTimeMillis())};
				
				this.save_to_db("tecaj", fields, types, values);
				
				System.out.println("Podaci su uspješno spremljeni za valutu "+this.valuta+", datum: "+this.on_date+"");
			}
			else 
			{
				System.out.println("Podaci su već ranije spremljeni za datum "+this.on_date+"");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public LinkedHashMap<String,Object> getConversion(String ulazna_valuta, String izlazna_valuta, Date datum)
	{
		db = new DatabaseManager();
        connection = db.getConnection();
        
        LinkedHashMap<String,Object> map = new LinkedHashMap();
		try {
			sql = "SELECT id, sifra, srednji FROM tecaj where sifra IN(?, ?) AND on_date = ? ";
			ps = (PreparedStatement) connection.prepareStatement(sql);
			ps.setString(1, ulazna_valuta);
			ps.setString(2, izlazna_valuta);
			ps.setDate(3, (java.sql.Date) datum);
			//System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
	            map.put(rs.getString("sifra"), rs.getString("srednji"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return map;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public String getValuta() {
		return valuta;
	}

	public void setValuta(String valuta) {
		this.valuta = valuta;
	}

	public String getJedinica() {
		return jedinica;
	}

	public void setJedinica(String jedinica) {
		this.jedinica = jedinica;
	}

	public String getKupovni() {
		return kupovni;
	}

	public void setKupovni(String kupovni) {
		this.kupovni = kupovni;
	}

	public String getSrednji() {
		return srednji;
	}

	public void setSrednji(String srednji) {
		this.srednji = srednji;
	}

	public String getProdajni() {
		return prodajni;
	}

	public void setProdajni(String prodajni) {
		this.prodajni = prodajni;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TecajnaLista other = (TecajnaLista) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
