package com.dev.user.controller;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.dev.user.model.TecajnaLista;

public class Conversion {

	public String srednji_tecaj_ulazna_valuta;
	public String srednji_tecaj_izlazna_valuta;
	public Double calculation;
	public String result;
	
	public String conversion(String ulazna_valuta, String izlazna_valuta, Date datum, Integer kolicina){
		
		TecajnaLista tj = new TecajnaLista();
		LinkedHashMap<String,Object> map = tj.getConversion(ulazna_valuta, izlazna_valuta, datum);
		
		srednji_tecaj_ulazna_valuta = null;
		srednji_tecaj_izlazna_valuta = null;
		
		for (Map.Entry entry : map.entrySet()) {
		    //System.out.println("valuta: "+entry.getKey() + ", srednji tecaj: " + entry.getValue());
    		if(entry.getKey().toString().equals(ulazna_valuta)){
    			srednji_tecaj_ulazna_valuta = entry.getValue().toString();
    		}
    		else if(entry.getKey().toString().equals(izlazna_valuta)){
    			srednji_tecaj_izlazna_valuta = entry.getValue().toString();
    		}
		}
		//System.out.println("Srednji tečaj ulazna valuta: "+srednji_tecaj_ulazna_valuta);
		//System.out.println("Srednji tečaj izlazna valuta: "+srednji_tecaj_izlazna_valuta);
		
		if(srednji_tecaj_ulazna_valuta != null && srednji_tecaj_izlazna_valuta != null)
		{
			srednji_tecaj_ulazna_valuta = srednji_tecaj_ulazna_valuta.replace(",", ".");
			srednji_tecaj_izlazna_valuta = srednji_tecaj_izlazna_valuta.replace(",", ".");
			
			calculation = (Double.parseDouble(srednji_tecaj_ulazna_valuta) / Double.parseDouble(srednji_tecaj_izlazna_valuta)) * kolicina;
			result = calculation.toString().replace(".", ",");
		}
		else 
		{
			result = "Ulazni podaci su neodgovarajući. \nProvjerite šifru ulazne i izlazne valute te unešeni datum za koji ste pokušali dobiti konverziju.";
		}
		//System.out.println(result);
		return result;
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((srednji_tecaj_ulazna_valuta == null) ? 0
						: srednji_tecaj_ulazna_valuta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conversion other = (Conversion) obj;
		if (srednji_tecaj_ulazna_valuta == null) {
			if (other.srednji_tecaj_ulazna_valuta != null)
				return false;
		} else if (!srednji_tecaj_ulazna_valuta
				.equals(other.srednji_tecaj_ulazna_valuta))
			return false;
		return true;
	}

}
